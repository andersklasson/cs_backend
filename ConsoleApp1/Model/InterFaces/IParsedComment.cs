﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Model
{
    public interface IParsedComment
    {
        int PersonId { get; set; }
        string CommentText { get; set; }
    }
}
