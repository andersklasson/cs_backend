﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ConsoleApp1.Model
{
    public interface IParsedPerson
    {

        public int PersonId { get; set; }

        public int Age { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string Color { get; set; }


    }
}
