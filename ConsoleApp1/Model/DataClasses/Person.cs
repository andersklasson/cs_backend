﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Model
{
    public class Person
    {
        public int PersonId;

        public string FirstName { get; set; }

        public int Age { get; set; }

        public string LastName { get; set; }
        public string Color { get; set; }

        public List<Comment> Comments { get; set; }

        public void AddComment(Comment comment)
        {
            Comments.Add(comment);
        }


        public List<Employment> Employments { get; set; }

        public Person()
        {
            Init();

        }

        public Person(IParsedPerson parsedPerson)
        {
            PersonId = parsedPerson.PersonId;
            FirstName = parsedPerson.FirstName;
            Age = parsedPerson.Age;
            Init();
        }

        private void Init()
        {
            Employments = new List<Employment>();
            Comments = new List<Comment>();
        }

        public override bool Equals(object obj)
        {
            return obj is Person person &&
                   PersonId == person.PersonId &&
                   FirstName == person.FirstName &&
                   Age == person.Age &&
                   LastName == person.LastName &&
                   Color == person.Color &&
                   EqualityComparer<List<Comment>>.Default.Equals(Comments, person.Comments) &&
                   EqualityComparer<List<Employment>>.Default.Equals(Employments, person.Employments);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(PersonId, FirstName, Age, LastName, Color, Comments, Employments);
        }
    }
}
