﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Model
{
    public class Employment
    {
        public int EmploymentId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public Company Company { get; set; }


    }
}
