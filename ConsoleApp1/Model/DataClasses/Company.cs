﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Model
{
    public class Company
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Company company &&
                   CompanyId == company.CompanyId;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CompanyId);
        }
    }
}
