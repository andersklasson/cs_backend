﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Model
{
    public class Comment
    {
        public int CommentId { get; set; }
        public int PersonId { get; set; }
        public string CommentText { get; set; }

        public Comment()
        {

        }
        public Comment(IParsedComment parsedComment)
        {
            PersonId = parsedComment.PersonId;
            CommentText = parsedComment.CommentText;
        }

        public override bool Equals(object obj)
        {
            return obj is Comment comment &&
                   CommentId == comment.CommentId;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CommentId);
        }
    }
}
