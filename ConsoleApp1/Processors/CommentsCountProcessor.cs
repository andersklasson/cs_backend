﻿using ConsoleApp1.Model;
using ConsoleApp1.Parsers;
using System;
using System.Collections.Generic;

namespace ConsoleApp1.Processors
{
    public class CommentsCountProcessor
    {


        public static List<Person> GetPersons(string path)
        {
            return ParserController.GetPersonsFromXML(path).ConvertAll(
                 new Converter<IParsedPerson, Person>(parsedPerson => new Person(parsedPerson)));
        }

        public static List<Comment> GetComments(string path)
        {
            return ParserController.GetCommentsFromCSV(path).ConvertAll(
                new Converter<IParsedComment, Comment>(parsedComment => new Comment(parsedComment)
                ));
        }

        public static void AddCommentsToPersons(List<Person> persons, List<Comment> comments)
        {
            comments.ForEach(c =>
            {
                try
                {
                    Person foundPerson = persons.Find(person => person.PersonId == c.PersonId);
                    foundPerson.AddComment(c);
                }
                catch
                {
                    Console.WriteLine("Could not find matching person for comment: " + Helpers.Helpers.StringifyObject(c));

                }



            });

        }

        public static void SortPersonsByCommentCount(List<Person> persons)
        {
            persons.Sort((a, b) =>
            {
                return b.Comments.Count - a.Comments.Count;
            });

        }


    }

}
