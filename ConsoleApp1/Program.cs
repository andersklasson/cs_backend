﻿using ConsoleApp1.Model;
using ConsoleApp1.Parsers;
using ConsoleApp1.Processors;
using System;
using System.Collections.Generic;
using ConsoleApp1.Constants;



namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintParsedCSVPersons();
            PrintPersonsByCommentsCount();
        }

        private static void PrintPersonsByCommentsCount()
        {
            Console.WriteLine("\n\nPrinting Persons by comments count  \n");
            List<Person> persons = CommentsCountProcessor.GetPersons(FilePaths.XML_PERSON_PATH);
            List<Comment> comments = CommentsCountProcessor.GetComments(FilePaths.CSV_COMMENT_PATH);

            CommentsCountProcessor.AddCommentsToPersons(persons, comments);
            CommentsCountProcessor.SortPersonsByCommentCount(persons);

            persons.ForEach(person =>
            {
                Console.WriteLine(
                    $"ID: { person.PersonId}, Comments count: {person.Comments.Count}"
                    );
            });
        }

        private static void PrintParsedCSVPersons()
        {
            Console.WriteLine("Printing Parsed Persons from\n\"{0}\"\n", FilePaths.CSV_PERSON_PATH);
            List<IParsedPerson> parsedCsvPersons = ParserController.GetPersonFromCSV(FilePaths.CSV_PERSON_PATH);
            parsedCsvPersons.ForEach(p =>
            {
                Console.WriteLine(Helpers.Helpers.StringifyObject(p));
            });
        }
    }
}
