﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1.Constants
{
    public static class FilePaths
    {


        private static readonly string INPUT_FILES_PATH_RELATIVE_PATH = "..\\Inputfiles";

        /*
         * File names
         */
        private static readonly string CSV_PERSON_FILE_NAME = "Data1.txt";
        private static readonly string XML_PERSON_FILE_NAME = "Data2.xml";
        private static readonly string CSV_COMMENTS_FILE_NAME = "Data3..txt";

        /*
         * Full Paths
         */
        public static readonly string CSV_PERSON_PATH = BuildPath(CSV_PERSON_FILE_NAME);
        public static readonly string CSV_COMMENT_PATH = BuildPath(CSV_COMMENTS_FILE_NAME);
        public static readonly string XML_PERSON_PATH = BuildPath(XML_PERSON_FILE_NAME);







        private static string BuildPath(string fileName)
        {

            return $"{ Directory.GetCurrentDirectory()}\\{INPUT_FILES_PATH_RELATIVE_PATH}\\{fileName}";
        }
    }
}
