﻿using ConsoleApp1.Model;
using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Parsers.CSV
{
    public class CSVComment : IParsedComment
    {

        [Name("PersonId")]
        public int PersonId { get; set; }

        [Name("Comment")]
        public string CommentText { get; set; }

    }
}
