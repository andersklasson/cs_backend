﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace ConsoleApp1.Parsers.CSV
{
    public class CSVParser
    {
        public static T[] ParseCSV<T>(string path, string delimiter) where T : class

        {
            using (var reader = new StreamReader(path))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.Delimiter = delimiter;
                T[] array = csv.GetRecords<T>().ToArray();
                return array;
            }
        }

    }
}

