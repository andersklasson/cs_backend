﻿using ConsoleApp1.Model;
using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ConsoleApp1.Parsers.CSV
{
    class CSVPerson : IParsedPerson

    {
        [Name("PersonId")]
        public int PersonId { get; set; }

        [Name("FirstName")]
        public string FirstName { get; set; }

        [Name("LastName")]
        public string LastName { get; set; }

        [Name("Age")]
        public int Age { get; set; }

        [Name("Color")]
        public string Color { get; set; }

    }
}
