﻿using ConsoleApp1.Model;
using ConsoleApp1.Parsers.CSV;
using ConsoleApp1.Parsers.XML;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Parsers
{
    public static class ParserController
    {
        public static List<IParsedPerson> GetPersonsFromXML(string path)
        {
            IParsedPerson[] persons = XMLParser.DeserializeToObject<XMLPersons>(path).Persons;
            return new List<IParsedPerson>(persons);
        }

        public static List<IParsedPerson> GetPersonFromCSV(string path)
        {
            IParsedPerson[] persons = CSVParser.ParseCSV<CSVPerson>(path, ";");
            return new List<IParsedPerson>(persons);
        }

        public static List<CSVComment> GetCommentsFromCSV(string path)
        {
            CSVComment[] comments = CSVParser.ParseCSV<CSVComment>(path, "\t");
            return new List<CSVComment>(comments);
        }


    }
}
