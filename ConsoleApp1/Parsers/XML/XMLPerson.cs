﻿using System.Xml.Serialization;

namespace ConsoleApp1.Model
{

    [XmlRoot(ElementName = "Data")]
    public class XMLPersons
    {

        [XmlArray("Persons")]
        public XMLPerson[] Persons { get; set; }


    }

    [XmlType("Person")]
    public class XMLPerson : IParsedPerson
    {

        [XmlAttribute("PersonId")]
        public int PersonId { get; set; }

        [XmlAttribute("age")]
        public int Age { get; set; }

        [XmlElement(ElementName = "FirstName")]
        public string FirstName { get; set; }

        [XmlElement(ElementName = "LastName")]
        public string LastName { get; set; }

        [XmlElement(ElementName = "Color")]
        public string Color { get; set; }




    }



}
