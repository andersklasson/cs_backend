﻿
using System.IO;
using System.Xml.Serialization;

namespace ConsoleApp1.Parsers.XML
{
    class XMLParser
    {

        public static T DeserializeToObject<T>(string filepath) where T : class
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));

            using (StreamReader sr = new StreamReader(filepath))
            {
                return (T)ser.Deserialize(sr);
            }
        }
    }
}
