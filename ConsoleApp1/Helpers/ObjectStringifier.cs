﻿using System;
using System.Reflection;


namespace ConsoleApp1.Helpers
{
    public static class Helpers
    {

        public static string StringifyObject(Object obj)
        {
            string Stringified = "";
            foreach (PropertyInfo p in obj.GetType().GetProperties())
            {

                Stringified += $"{p.Name}: {p.GetValue(obj)}, ";
            }
            return Stringified;

        }
    }

}
